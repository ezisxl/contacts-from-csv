//
//  CFCViewController.m
//  ContactsFromCsv
//
//  Created by Ansis on 4/2/13.
//  Copyright (c) 2013 Ansis. All rights reserved.
//

#import "CFCViewController.h"
#import <AddressBook/AddressBook.h>
#import "UIAlertView+BlockExtensions.h"


/*
 CSV pattern:
 
 Firstname,Lastname,Email,Phone
 
 */

#define FILENAME @"my_google"

@interface CFCViewController ()
{
    UILabel *pLabel;
    ABAddressBookRef addressBook;
}

@end

@implementation CFCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    [self.view setBackgroundColor: [UIColor scrollViewTexturedBackgroundColor] ];
    
    int h = 50;
    pLabel = [[UILabel alloc] initWithFrame: CGRectMake(0,
                                                       self.view.frame.size.height/2 - h/2,
                                                       self.view.frame.size.width,
                                                       h)];
    
    [pLabel setFont: [UIFont boldSystemFontOfSize: 32] ];
    [pLabel setTextColor: [UIColor lightTextColor] ];
    [pLabel setTextAlignment: NSTextAlignmentCenter];
    [pLabel setBackgroundColor: [UIColor clearColor] ];
    [pLabel setShadowColor: [UIColor colorWithWhite: 0 alpha: 0.6] ];
    [pLabel setShadowOffset: CGSizeMake(0, 1)];
    [pLabel setAdjustsFontSizeToFitWidth: YES];

    [self.view addSubview: pLabel];

    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            [self pProcessStuff];            
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        [self pProcessStuff];
    } else {
        [self pShowError: @"user has denied acces to contacts for this app"];
    }

}

- (void)pShowError:(NSString*)string
{
    
    [pLabel setTextColor: [UIColor colorWithRed:0.918 green:0.255 blue:0.282 alpha:1.000] ];
    [pLabel setText: string];
    
}

- (void)pProcessStuff
{
    
    [[[UIAlertView alloc] initWithTitle: @"Add multiple contacts"
                                message: @"This will load contacts from CSV to your contacts.\nContinue?"
                        completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                            switch (buttonIndex) {
                                    
                                case 0:
                                {
                                    
                                    dispatch_queue_t concurrentQueue1 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                    dispatch_async(concurrentQueue1,
                                                   ^{
                                                       
                                                       NSString *filePath = [[NSBundle mainBundle] pathForResource: FILENAME ofType:@"csv"];
                                                       NSData *myData = [NSData dataWithContentsOfFile:filePath];
                                                       if (!myData) {
                                                           dispatch_async(dispatch_get_main_queue(),
                                                                          ^{
                                                                              [self pShowError: @"csv file not found"];
                                                                          });
                                                           return;
                                                       }
                                                       
                                                       NSString *data = [[NSString alloc] initWithData: myData encoding: NSUTF8StringEncoding];
                                                       
                                                       NSArray *lines = [data componentsSeparatedByString:@"\n"];
                                                       
                                                       int done = 0;
                                                       for (NSString *aLine in lines){
                                                           
                                                           NSArray *components = [aLine componentsSeparatedByString:@","];
                                                           
                                                           NSString *fname =  [[components objectAtIndex: 0] length] > 0  ? [components objectAtIndex: 0] : @"";
                                                           NSString *lname =  [[components objectAtIndex: 1] length] > 0  ? [components objectAtIndex: 1] : @"";
                                                           NSString *email =  [[components objectAtIndex: 2] length] > 0  ? [components objectAtIndex: 2] : @"";
                                                           NSString *phone =  [[components objectAtIndex: 3] length] > 0  ? [components objectAtIndex: 3] : @"";
                                                           
                                                           NSDictionary *personData = @{
                                                                                        @"fName" : fname,
                                                                                        @"lName" : lname,
                                                                                        @"email" : email,
                                                                                        @"phone" : phone,
                                                                                        };
                                                           
                                                           if ([[components objectAtIndex: 0] length] > 0){
                                                               [self pAddPersonWithData: personData];
                                                               dispatch_async(dispatch_get_main_queue(),
                                                                              ^{
                                                                                  [pLabel setText: [NSString stringWithFormat: @"%i of %i", done, [lines count] ] ];
                                                                              });
                                                           }
                                                           done++;
                                                           
                                                           
                                                       }
                                                       
                                                       dispatch_async(dispatch_get_main_queue(),
                                                                      ^{
                                                                          [pLabel setText: [NSString stringWithFormat: @"%i contacts added",  [lines count] ] ];
                                                                      });
                                                       
                                                   });
                                 
                                    
                                }
                                    break;
                                    
                                    case 1:
                                        [self pShowError: @"user cancelled"];
                                    break;
                                    
                            }
                        }
                      cancelButtonTitle: @"Yes"
                      otherButtonTitles: @"No", nil] show];
    
}

- (void)pAddPersonWithData:(NSDictionary*)personData
{
    if (!personData) return;

    NSString *phone = [NSString stringWithString: [personData objectForKey:@"phone"] ];
    NSString *fName = [NSString stringWithString: [personData objectForKey:@"fName"] ];
    NSString *lName = [NSString stringWithString: [personData objectForKey:@"lName"] ];
    NSString *email = [NSString stringWithString: [personData objectForKey:@"email"] ];
    
    ABRecordRef person = ABPersonCreate();
    
    // firstname
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)(fName) , nil);
    // lastname
    ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFTypeRef)(lName), nil);
    // email
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)(phone), kABPersonPhoneMobileLabel, NULL);
    ABRecordSetValue(person, kABPersonPhoneProperty, multiPhone,nil);
    // phone
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(email), kABWorkLabel, NULL);
    ABRecordSetValue(person, kABPersonEmailProperty, multiEmail, nil);
    
    ABAddressBookAddRecord(addressBook, person, nil);
    ABAddressBookSave(addressBook, nil);    

    CFRelease(person);
    
}
@end
