//
//  CFCAppDelegate.h
//  ContactsFromCsv
//
//  Created by Ansis on 4/2/13.
//  Copyright (c) 2013 Ansis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CFCViewController;

@interface CFCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) CFCViewController *viewController;

@end
