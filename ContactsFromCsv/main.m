//
//  main.m
//  ContactsFromCsv
//
//  Created by Ansis on 4/2/13.
//  Copyright (c) 2013 Ansis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CFCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CFCAppDelegate class]));
    }
}
