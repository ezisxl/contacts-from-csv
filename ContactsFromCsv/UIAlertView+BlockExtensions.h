//
//  UIAlertView+BlockExtensions.h
//  aiSTEPS
//
//  Created by Ansis on 3/26/13.
//  Copyright (c) 2013 FTC Tele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (BlockExtensions) <UIAlertViewDelegate>
- (id)initWithTitle:(NSString *)title message:(NSString *)message completionBlock:(void (^)(NSUInteger buttonIndex, UIAlertView *alertView))block cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;
@end
